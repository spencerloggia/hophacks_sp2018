# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
This is a repository for a web-based Hophacks Solution for Spring 2018!
### How do I get set up? ###

* Summary of set up
project directory must be installed on a windows / mac / linux apache web server, or on a remote host domain.
The server must also be running an SQL database, development used phpMyAdmin
if installed on a remote server, remote access is direct through ipv4 connection protocol

* Dependencies
HTML 5
PHP 7
J-query
bootstrap.js
bootstrap.css
phpMyAdmin

* Database configuration
phpMyAdmin SQL database serves data for project
* How to run tests
behat QA should be implemented if time permits
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
Only people who should be contributing are members of our Hackathon team! Repository is public since no one likely will try to access :)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact